export {};
/**
 * Функция слияния двух объектов
 * @param {T1} obj1 
 * @param {T2} obj2 
 * @returns {T1 & T2}
 */
function merge<T1, T2>(obj1: T1, obj2: T2): T1 & T2 {
  return {
    ...obj1,
    ...obj2,
  };
}
